package com.example.managementCashier.swagger;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class SwaggerPageable {

    @ApiParam(example = "0")
    private Integer size;

    @ApiParam(example = "0")
    private Integer page;
}
