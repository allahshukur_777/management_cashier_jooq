package com.example.managementCashier.exception;

public class ZoneNotFoundException extends RuntimeException {

    public ZoneNotFoundException(Long id) {
        super(id + " Zone not found");
    }
}
