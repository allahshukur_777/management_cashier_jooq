package com.example.managementCashier.exception;

public class FreezeStatusNotFoundException extends RuntimeException {

    public FreezeStatusNotFoundException(Long id) {
        super(id + " Freeze Status not found");
    }
}
