package com.example.managementCashier.exception;

public class StatusNotFoundException extends RuntimeException {

    public StatusNotFoundException(int id) {
        super(id + " Status not found");
    }
}
