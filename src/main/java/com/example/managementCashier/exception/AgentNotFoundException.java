package com.example.managementCashier.exception;

public class AgentNotFoundException extends RuntimeException {

    public AgentNotFoundException(Long id) {
        super(id + " Agent not found");
    }
}
