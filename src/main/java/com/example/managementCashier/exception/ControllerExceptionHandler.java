package com.example.managementCashier.exception;

import java.time.DateTimeException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(OperatorNotFoundException.class)
    public ResponseEntity<?> operatorNotFoundException(OperatorNotFoundException onf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                onf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AgentNotFoundException.class)
    public ResponseEntity<?> agentNotFoundException(AgentNotFoundException anf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                anf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CashierNotFoundException.class)
    public ResponseEntity<?> cashierNotFoundException(CashierNotFoundException cnf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                cnf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ZoneNotFoundException.class)
    public ResponseEntity<?> zoneNotFoundException(ZoneNotFoundException znf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                znf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CitiesNotFoundException.class)
    public ResponseEntity<?> citiesNotFoundException(CitiesNotFoundException cnf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                cnf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RegionNotFoundException.class)
    public ResponseEntity<?> regionNotFoundException(RegionNotFoundException rnf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                rnf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(StatusNotFoundException.class)
    public ResponseEntity<?> statusNotFoundException(StatusNotFoundException snf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                snf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FreezeStatusNotFoundException.class)
    public ResponseEntity<?> freezeStatusNotFoundException(FreezeStatusNotFoundException fnf) {
        Integer httpStatus = HttpStatus.BAD_REQUEST.value();
        ErrorMessage errorMessage = new ErrorMessage(
                httpStatus,
                fnf.getMessage());
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String message = error.getDefaultMessage();
            errors.put(fieldName, message);
        });
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }
}
