package com.example.managementCashier.exception;

import lombok.Data;

@Data
public class ErrorMessage {
    private final Integer statusCode;
    private final String message;

    public ErrorMessage(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }
}
