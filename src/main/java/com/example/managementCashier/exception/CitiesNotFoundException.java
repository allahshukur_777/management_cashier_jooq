package com.example.managementCashier.exception;

public class CitiesNotFoundException extends RuntimeException {

    public CitiesNotFoundException(Long id) {
        super(id + " Cities not found");
    }
}
