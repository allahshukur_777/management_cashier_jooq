package com.example.managementCashier.exception;

public class RegionNotFoundException extends RuntimeException {

    public RegionNotFoundException(Long id) {
        super(id + " Region not found");
    }
}
