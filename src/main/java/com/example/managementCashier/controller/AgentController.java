package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.service.AgentService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/agent")
public class AgentController {

    private final AgentService agentService;

    @GetMapping
    public List<AgentDto> getAll(@PageableDefault(size = 7) Pageable pageable) {
        return agentService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public AgentDto findById(@PathVariable Long id) {
        return agentService.get(id);
    }

    @GetMapping("/name/{id}")
    public Object getName(@PathVariable Long id) {
        return agentService.getName(id);
    }

    @PostMapping
    public AgentSaveDto save(@Valid @RequestBody AgentSaveDto agentSave) {
        return agentService.save(agentSave);
    }

    @PutMapping("/{id}")
    public AgentUpdateDto update(@PathVariable Long id, @Valid @RequestBody AgentUpdateDto agentUpdate) {
        return agentService.update(id, agentUpdate);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        agentService.deleteById(id);
    }
}
