package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.service.OperatorService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/operator")
public class OperatorController {

    private final OperatorService operatorService;

    @GetMapping
    public List<OperatorDto> getAll(@PageableDefault(size = 20) Pageable pageable) {
        return operatorService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public OperatorDto get(@PathVariable Long id) {
        return operatorService.get(id);
    }

    @PostMapping
    public OperatorSaveDto save(@Valid @RequestBody OperatorSaveDto operatorSave) {
        return operatorService.save(operatorSave);
    }

    @PutMapping("/{id}")
    public OperatorUpdateDto update(@PathVariable Long id, @Valid @RequestBody OperatorUpdateDto operatorUpdate) {
        return operatorService.update(id, operatorUpdate);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        operatorService.deleteById(id);
    }
}
