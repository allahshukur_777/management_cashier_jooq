package com.example.managementCashier.controller;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.service.CashierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cashier")
public class CashierController {

    private final CashierService cashierService;

    @GetMapping
    public List<CashierDto> getAll(@PageableDefault(size = 5) Pageable pageable) {
        return cashierService.getAll(pageable);
    }

    @GetMapping("/{id}")
    public CashierDto get(@PathVariable Long id) {
        return cashierService.get(id);
    }

    @PostMapping
    public CashierSaveDto save(@Valid @RequestBody CashierSaveDto cashierSave) {
        return cashierService.save(cashierSave);
    }

    @PutMapping("/{id}")
    public CashierUpdateDto update(@PathVariable Long id, @Valid @RequestBody CashierUpdateDto cashierUpdate) {
        return cashierService.update(id, cashierUpdate);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        cashierService.deleteById(id);
    }
}
