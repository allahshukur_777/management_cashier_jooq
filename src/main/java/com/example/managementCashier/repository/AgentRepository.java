package com.example.managementCashier.repository;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AgentRepository {

    List<AgentDto> findAll(Pageable pageable);

    AgentDto findById(Long id);

    AgentNameDto findName(Long id);

    AgentSaveDto save(AgentSaveDto agentSaveDto);

    AgentUpdateDto update(Long id, AgentUpdateDto agentUpdateDto);

    void deleteById(Long id);
}
