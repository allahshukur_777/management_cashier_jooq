package com.example.managementCashier.repository;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import org.springframework.data.domain.Pageable;
import java.util.List;

public interface OperatorRepository {

    List<OperatorDto> findAll(Pageable pageable);

    OperatorDto findById(Long id);

    OperatorSaveDto save(OperatorSaveDto operatorSaveDto);

    OperatorUpdateDto update(Long id, OperatorUpdateDto operatorUpdateDto);

    void deleteById(Long id);
}
