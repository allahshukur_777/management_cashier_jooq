package com.example.managementCashier.repository.impl;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.exception.OperatorNotFoundException;
import com.example.managementCashier.mapper.operator.OperatorMapper;
import com.example.managementCashier.repository.OperatorRepository;

import java.util.List;

import static jooq.tables.Operator.OPERATOR;
import static jooq.tables.Agent.AGENT;
import static jooq.tables.Cashier.CASHIER;

import com.example.managementCashier.util.BigDecimalUtil;
import com.example.managementCashier.util.DateUtil;
import jooq.tables.records.OperatorRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class OperatorRepositoryImpl implements OperatorRepository {

    private final DSLContext dslContext;
    private final OperatorMapper operatorMapper;

    @Override
    public List<OperatorDto> findAll(Pageable pageable) {
        Result<Record> recordResult = dslContext.select()
                .from(OPERATOR)
                .leftJoin(AGENT).on(OPERATOR.ID.equal(AGENT.OPERATOR_ID))
                .leftJoin(CASHIER).on(AGENT.ID.equal(CASHIER.AGENT_ID))
                .where(OPERATOR.DELETE.equal(false)
                        .and(AGENT.DELETE.equal(false)
                                .and(CASHIER.DELETE.equal(false))))
                .or(OPERATOR.DELETE.equal(false)
                        .and(AGENT.DELETE.equal(false)))
                .or(OPERATOR.DELETE.equal(false))
                .orderBy(OPERATOR.ID, AGENT.ID, CASHIER.ID)
                .offset(pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .fetch();
        return operatorMapper.toOperatorDto(recordResult);
    }

    @Override
    public OperatorDto findById(Long id) {
        Result<Record> recordResult = dslContext.select()
                .from(OPERATOR)
                .leftJoin(AGENT).on(OPERATOR.ID.equal(AGENT.OPERATOR_ID))
                .leftJoin(CASHIER).on(AGENT.ID.equal(CASHIER.AGENT_ID))
                .where(OPERATOR.DELETE.equal(false)
                        .and(AGENT.DELETE.equal(false)
                                .and(CASHIER.DELETE.equal(false)
                                        .and(OPERATOR.ID.equal(id)))))
                .or(OPERATOR.DELETE.equal(false)
                        .and(AGENT.DELETE.equal(false)
                                .and(OPERATOR.ID.equal(id))))
                .or(OPERATOR.DELETE.equal(false)
                        .and(OPERATOR.ID.equal(id)))
                .orderBy(AGENT.ID, CASHIER.ID)
                .fetch();
        if (recordResult.size() == 0) throw new OperatorNotFoundException(id);
        return operatorMapper.toOperatorDto(recordResult).get(0);
    }

    @Override
    public OperatorSaveDto save(OperatorSaveDto operatorSave) {
        OperatorRecord operatorRecord = dslContext.insertInto(OPERATOR)
                .set(OPERATOR.NAME, operatorSave.getName())
                .set(OPERATOR.TYPE, operatorSave.getType())
                .set(OPERATOR.HEADQUARTER_ADDRESS, operatorSave.getHeadquarterAddress())
                .set(OPERATOR.EMAIL_ADDRESS, operatorSave.getEmailAddress())
                .set(OPERATOR.WEB_ADDRESS, operatorSave.getWebAddress())
                .set(OPERATOR.PHONE_NUMBER, operatorSave.getPhoneNumber())
                .set(OPERATOR.FAX_NUMBER, operatorSave.getFaxNumber())
                .set(OPERATOR.MAX_STAKE, BigDecimalUtil.value(operatorSave.getMaxStake()))
                .set(OPERATOR.MIN_STAKE, BigDecimalUtil.value(operatorSave.getMinStake()))
                .set(OPERATOR.PAYOUT_LIMIT, BigDecimalUtil.value(operatorSave.getPayoutLimit()))
                .set(OPERATOR.MAX_ISSUE_AMOUNT_PER_VOUCHER,
                        BigDecimalUtil.value(operatorSave.getMaxIssueAmountPerVoucher()))
                .set(OPERATOR.AVAILABLE_CREDIT_LIMIT,
                        BigDecimalUtil.value(operatorSave.getAvailableCreditLimit()))
                .set(OPERATOR.TICKET_EXPIRATION_PERIOD,
                        BigDecimalUtil.value(operatorSave.getTicketExpirationPeriod()))
                .set(OPERATOR.STATUS, StatusEnum.getEnum(operatorSave.getStatus()).getId())
                .set(OPERATOR.CREATION_DATE, DateUtil.getDate(operatorSave.getCreationDate()))
                .returning()
                .fetchOne();
        return operatorMapper.toOperatorSaveDto(operatorRecord);
    }

    @Override
    public OperatorUpdateDto update(Long id, OperatorUpdateDto operatorUpdate) {
        OperatorRecord operatorRecord = dslContext.update(OPERATOR)
                .set(OPERATOR.NAME, operatorUpdate.getName())
                .set(OPERATOR.TYPE, operatorUpdate.getType())
                .set(OPERATOR.HEADQUARTER_ADDRESS, operatorUpdate.getHeadquarterAddress())
                .set(OPERATOR.EMAIL_ADDRESS, operatorUpdate.getEmailAddress())
                .set(OPERATOR.WEB_ADDRESS, operatorUpdate.getWebAddress())
                .set(OPERATOR.PHONE_NUMBER, operatorUpdate.getPhoneNumber())
                .set(OPERATOR.FAX_NUMBER, operatorUpdate.getFaxNumber())
                .set(OPERATOR.MAX_STAKE, BigDecimalUtil.value(operatorUpdate.getMaxStake()))
                .set(OPERATOR.MIN_STAKE, BigDecimalUtil.value(operatorUpdate.getMinStake()))
                .set(OPERATOR.PAYOUT_LIMIT, BigDecimalUtil.value(operatorUpdate.getPayoutLimit()))
                .set(OPERATOR.MAX_ISSUE_AMOUNT_PER_VOUCHER,
                        BigDecimalUtil.value(operatorUpdate.getMaxIssueAmountPerVoucher()))
                .set(OPERATOR.AVAILABLE_CREDIT_LIMIT,
                        BigDecimalUtil.value(operatorUpdate.getAvailableCreditLimit()))
                .set(OPERATOR.TICKET_EXPIRATION_PERIOD,
                        BigDecimalUtil.value(operatorUpdate.getTicketExpirationPeriod()))
                .set(OPERATOR.STATUS, StatusEnum.getEnum(operatorUpdate.getStatus()).getId())
                .set(OPERATOR.CREATION_DATE, DateUtil.getDate(operatorUpdate.getCreationDate()))
                .where(OPERATOR.ID.equal(id))
                .returning()
                .fetchOne();
        if (operatorRecord == null) throw new OperatorNotFoundException(id);
        return operatorMapper.toOperatorUpdateDto(operatorRecord);
    }

    @Override
    public void deleteById(Long id) {
        int[] batch = dslContext.batch(
                        dslContext.update(OPERATOR)
                                .set(OPERATOR.DELETE, true)
                                .where(OPERATOR.DELETE.equal(false)
                                        .and(OPERATOR.ID.equal(id))),
                        dslContext.update(AGENT)
                                .set(AGENT.DELETE, true)
                                .where(AGENT.DELETE.equal(false)
                                        .and(AGENT.OPERATOR_ID.equal(id))),
                        dslContext.update(CASHIER)
                                .set(CASHIER.DELETE, true)
                                .from(AGENT)
                                .where(CASHIER.DELETE.equal(false)
                                        .and(CASHIER.AGENT_ID.equal(AGENT.ID)
                                                .and(AGENT.OPERATOR_ID.equal(id))))
                )
                .execute();
        if (batch[0] == 0) throw new OperatorNotFoundException(id);
    }
}