package com.example.managementCashier.repository.impl;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.exception.CashierNotFoundException;
import com.example.managementCashier.mapper.cashier.CashierMapper;
import com.example.managementCashier.repository.CashierRepository;

import java.util.List;
import java.util.stream.Collectors;

import com.example.managementCashier.util.DateUtil;
import com.example.managementCashier.util.BigDecimalUtil;
import com.example.managementCashier.util.ExceptionUtil;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import static jooq.tables.Cashier.CASHIER;

@Repository
@RequiredArgsConstructor
public class CashierRepositoryImpl implements CashierRepository {

    private final DSLContext dslContext;
    private final CashierMapper cashierMapper;

    @Override
    public List<CashierDto> findAll(Pageable pageable) {
        return dslContext.selectFrom(CASHIER)
                .where(CASHIER.DELETE.equal(false))
                .orderBy(CASHIER.ID)
                .offset(pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .fetchInto(Cashier.class)
                .stream()
                .map(cashierMapper::toCashierDto)
                .collect(Collectors.toList());
    }

    @Override
    public CashierDto findById(Long id) {
        Cashier cashier = dslContext.selectFrom(CASHIER)
                .where(CASHIER.DELETE.equal(false)
                        .and(CASHIER.ID.equal(id)))
                .fetchOneInto(Cashier.class);
        if (cashier == null) throw new CashierNotFoundException(id);
        return cashierMapper.toCashierDto(cashier);
    }

    @Override
    public CashierSaveDto save(CashierSaveDto cashierSave) {
        try {
            CashierRecord cashierRecord = dslContext.insertInto(CASHIER)
                    .set(CASHIER.CASHIER_CODE, cashierSave.getCashierCode())
                    .set(CASHIER.PROVIDER_ID, cashierSave.getProviderId())
                    .set(CASHIER.FULL_NAME, cashierSave.getFullName())
                    .set(CASHIER.MOBILE, cashierSave.getMobile())
                    .set(CASHIER.PHONE, cashierSave.getPhone())
                    .set(CASHIER.EMAIL, cashierSave.getEmail())
                    .set(CASHIER.SALES_REPRESENTATIVE, cashierSave.getSalesRepresentative())
                    .set(CASHIER.ZONE, cashierSave.getZone())
                    .set(CASHIER.CITIES, cashierSave.getCities())
                    .set(CASHIER.REGION, cashierSave.getRegion())
                    .set(CASHIER.ADDRESS, cashierSave.getAddress())
                    .set(CASHIER.MAC_ADDRESS, cashierSave.getMacAddress())
                    .set(CASHIER.NEXT_PERMANENT_BALANCE, BigDecimalUtil.value(cashierSave.getNextPermanentBalance()))
                    .set(CASHIER.CURRENT_BALANCE, BigDecimalUtil.value(cashierSave.getCurrentBalance()))
                    .set(CASHIER.DEBT_CREDIT, BigDecimalUtil.value(cashierSave.getDebtCredit()))
                    .set(CASHIER.EXTRA_DEBT_CREDIT, BigDecimalUtil.value(cashierSave.getExtraDebtCredit()))
                    .set(CASHIER.MIN_STAKE, BigDecimalUtil.value(cashierSave.getMinStake()))
                    .set(CASHIER.MAX_STAKE, BigDecimalUtil.value(cashierSave.getMaxStake()))
                    .set(CASHIER.BET_TICKET_PAYOUT_LIMIT, BigDecimalUtil.value(cashierSave.getBetTicketPayoutLimit()))
                    .set(CASHIER.VOUCHER_PAYOUT_LIMIT, BigDecimalUtil.value(cashierSave.getVoucherPayoutLimit()))
                    .set(CASHIER.STATUS, StatusEnum.getEnum(cashierSave.getStatus()).getId())
                    .set(CASHIER.FREEZE_STATUS, cashierSave.getFreezeStatus())
                    .set(CASHIER.USER_NAME, cashierSave.getUserName())
                    .set(CASHIER.PASSWORD, cashierSave.getPassword())
                    .set(CASHIER.LAST_LOGIN, DateUtil.getDate(cashierSave.getLastLogin()))
                    .set(CASHIER.LAST_LOGOUT, DateUtil.getDate(cashierSave.getLastLogout()))
                    .set(CASHIER.CREATION_DATE, DateUtil.getDate(cashierSave.getCreationDate()))
                    .set(CASHIER.AGENT_ID, cashierSave.getAgentId())
                    .returning()
                    .fetchOne();
            return cashierMapper.toCashierSaveDto(cashierRecord);
        } catch (Exception ex) {
            ExceptionUtil.cashierFkeyException(ex, cashierSave.getZone(), cashierSave.getCities(),
                    cashierSave.getRegion(), cashierSave.getFreezeStatus(), cashierSave.getAgentId());
            throw new RuntimeException();
        }
    }

    @Override
    public CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdate) {
        try {
            CashierRecord cashierRecord = dslContext.update(CASHIER)
                    .set(CASHIER.CASHIER_CODE, cashierUpdate.getCashierCode())
                    .set(CASHIER.PROVIDER_ID, cashierUpdate.getProviderId())
                    .set(CASHIER.FULL_NAME, cashierUpdate.getFullName())
                    .set(CASHIER.MOBILE, cashierUpdate.getMobile())
                    .set(CASHIER.PHONE, cashierUpdate.getPhone())
                    .set(CASHIER.EMAIL, cashierUpdate.getEmail())
                    .set(CASHIER.SALES_REPRESENTATIVE, cashierUpdate.getSalesRepresentative())
                    .set(CASHIER.ZONE, cashierUpdate.getZone())
                    .set(CASHIER.CITIES, cashierUpdate.getCities())
                    .set(CASHIER.REGION, cashierUpdate.getRegion())
                    .set(CASHIER.ADDRESS, cashierUpdate.getAddress())
                    .set(CASHIER.MAC_ADDRESS, cashierUpdate.getMacAddress())
                    .set(CASHIER.NEXT_PERMANENT_BALANCE, BigDecimalUtil.value(cashierUpdate.getNextPermanentBalance()))
                    .set(CASHIER.CURRENT_BALANCE, BigDecimalUtil.value(cashierUpdate.getCurrentBalance()))
                    .set(CASHIER.DEBT_CREDIT, BigDecimalUtil.value(cashierUpdate.getDebtCredit()))
                    .set(CASHIER.EXTRA_DEBT_CREDIT, BigDecimalUtil.value(cashierUpdate.getExtraDebtCredit()))
                    .set(CASHIER.MIN_STAKE, BigDecimalUtil.value(cashierUpdate.getMinStake()))
                    .set(CASHIER.MAX_STAKE, BigDecimalUtil.value(cashierUpdate.getMaxStake()))
                    .set(CASHIER.BET_TICKET_PAYOUT_LIMIT, BigDecimalUtil.value(cashierUpdate.getBetTicketPayoutLimit()))
                    .set(CASHIER.VOUCHER_PAYOUT_LIMIT, BigDecimalUtil.value(cashierUpdate.getVoucherPayoutLimit()))
                    .set(CASHIER.STATUS, StatusEnum.getEnum(cashierUpdate.getStatus()).getId())
                    .set(CASHIER.FREEZE_STATUS, cashierUpdate.getFreezeStatus())
                    .set(CASHIER.USER_NAME, cashierUpdate.getUserName())
                    .set(CASHIER.PASSWORD, cashierUpdate.getPassword())
                    .set(CASHIER.LAST_LOGIN, DateUtil.getDate(cashierUpdate.getLastLogin()))
                    .set(CASHIER.LAST_LOGOUT, DateUtil.getDate(cashierUpdate.getLastLogout()))
                    .set(CASHIER.CREATION_DATE, DateUtil.getDate(cashierUpdate.getCreationDate()))
                    .set(CASHIER.AGENT_ID, cashierUpdate.getAgentId())
                    .where(CASHIER.ID.equal(id))
                    .returning()
                    .fetchOne();
            if (cashierRecord == null) throw new CashierNotFoundException(id);
            return cashierMapper.toCashierUpdateDto(cashierRecord);
        } catch (Exception ex) {
            ExceptionUtil.cashierFkeyException(ex, cashierUpdate.getZone(), cashierUpdate.getCities(),
                    cashierUpdate.getRegion(), cashierUpdate.getFreezeStatus(), cashierUpdate.getAgentId());
            throw new RuntimeException();
        }
    }

    @Override
    public void deleteById(Long id) {
        int cashierSize = dslContext.update(CASHIER)
                .set(CASHIER.DELETE, true)
                .where(CASHIER.DELETE.equal(false)
                        .and(CASHIER.ID.equal(id)))
                .execute();
        if (cashierSize == 0) throw new CashierNotFoundException(id);
    }
}