package com.example.managementCashier.repository.impl;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.exception.AgentNotFoundException;
import com.example.managementCashier.mapper.agent.AgentMapper;
import com.example.managementCashier.repository.AgentRepository;

import java.util.List;

import static jooq.tables.Agent.AGENT;
import static jooq.tables.Cashier.CASHIER;

import com.example.managementCashier.util.DateUtil;
import com.example.managementCashier.util.BigDecimalUtil;
import com.example.managementCashier.util.ExceptionUtil;
import jooq.tables.records.AgentRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class AgentRepositoryImpl implements AgentRepository {

    private final DSLContext dslContext;
    private final AgentMapper agentMapper;

    @Override
    public List<AgentDto> findAll(Pageable pageable) {
        Result<Record> recordResult = dslContext.select()
                .from(AGENT)
                .leftJoin(CASHIER).on(AGENT.ID.equal(CASHIER.AGENT_ID))
                .where(AGENT.DELETE.equal(false)
                        .and(CASHIER.DELETE.equal(false)))
                .or((AGENT.DELETE.equal(false)))
                .orderBy(AGENT.ID, CASHIER.ID)
                .offset(pageable.getPageNumber())
                .limit(pageable.getPageSize())
                .fetch();
        return agentMapper.toAgentDto(recordResult);
    }

    @Override
    public AgentDto findById(Long id) {
        Result<Record> recordResult = dslContext.select()
                .from(AGENT)
                .leftJoin(CASHIER).on(AGENT.ID.equal(CASHIER.AGENT_ID))
                .where(AGENT.DELETE.equal(false)
                        .and(CASHIER.DELETE.equal(false))
                        .and(AGENT.ID.equal(id)))
                .or(AGENT.DELETE.equal(false)
                        .and(AGENT.ID.equal(id)))
                .orderBy(CASHIER.ID)
                .fetch();
        if (recordResult.size() == 0) throw new AgentNotFoundException(id);
        return agentMapper.toAgentDto(recordResult).get(0);
    }

    @Override
    public AgentNameDto findName(Long id) {
        String agentName = dslContext.select(AGENT.FULL_NAME)
                .from(AGENT)
                .where(AGENT.DELETE.equal(false)
                        .and(AGENT.ID.equal(id)))
                .orderBy(AGENT.ID)
                .fetchOneInto(String.class);
        if (agentName == null) throw new AgentNotFoundException(id);
        return agentMapper.toAgentNameDto(agentName);
    }

    @Override
    public AgentSaveDto save(AgentSaveDto agentSave) {
        try {
            AgentRecord agentRecord = dslContext.insertInto(AGENT)
                    .set(AGENT.AGENT_CODE, agentSave.getAgentCode())
                    .set(AGENT.FULL_NAME, agentSave.getFullName())
                    .set(AGENT.ID_NUMBER, agentSave.getIdNumber())
                    .set(AGENT.VOEN, agentSave.getVoen())
                    .set(AGENT.PHONE, agentSave.getPhone())
                    .set(AGENT.MOBILE, agentSave.getMobile())
                    .set(AGENT.EMAIL, agentSave.getEmail())
                    .set(AGENT.SALES_REPRESENTATIVE, agentSave.getSalesRepresentative())
                    .set(AGENT.CITIES, agentSave.getCities())
                    .set(AGENT.ADDRESS, agentSave.getAddress())
                    .set(AGENT.TOTAL_PERMANENT_BALANCE, BigDecimalUtil.value(agentSave.getTotalPermanentBalance()))
                    .set(AGENT.DEBT_CREDIT, BigDecimalUtil.value(agentSave.getDebtCredit()))
                    .set(AGENT.EXTRA_DEBT_CREDIT, BigDecimalUtil.value(agentSave.getExtraDebtCredit()))
                    .set(AGENT.STATUS, StatusEnum.getEnum(agentSave.getStatus()).getId())
                    .set(AGENT.USER_NAME, agentSave.getUserName())
                    .set(AGENT.PASSWORD, agentSave.getPassword())
                    .set(AGENT.LAST_LOGIN, DateUtil.getDate(agentSave.getLastLogin()))
                    .set(AGENT.CREATION_DATE, DateUtil.getDate(agentSave.getCreationDate()))
                    .set(AGENT.TYPE, agentSave.getType())
                    .set(AGENT.OPERATOR_ID, agentSave.getOperatorId())
                    .returning()
                    .fetchOne();
            return agentMapper.toAgentSaveDto(agentRecord);
        } catch (Exception ex) {
            ExceptionUtil.agentFkeyException(ex, agentSave.getCities(), agentSave.getOperatorId());
            throw new RuntimeException();
        }
    }

    @Override
    public AgentUpdateDto update(Long id, AgentUpdateDto agentUpdate) {
        try {
            AgentRecord agentRecord = dslContext.update(AGENT)
                    .set(AGENT.AGENT_CODE, agentUpdate.getAgentCode())
                    .set(AGENT.FULL_NAME, agentUpdate.getFullName())
                    .set(AGENT.ID_NUMBER, agentUpdate.getIdNumber())
                    .set(AGENT.VOEN, agentUpdate.getVoen())
                    .set(AGENT.PHONE, agentUpdate.getPhone())
                    .set(AGENT.MOBILE, agentUpdate.getMobile())
                    .set(AGENT.EMAIL, agentUpdate.getEmail())
                    .set(AGENT.SALES_REPRESENTATIVE, agentUpdate.getSalesRepresentative())
                    .set(AGENT.CITIES, agentUpdate.getCities())
                    .set(AGENT.ADDRESS, agentUpdate.getAddress())
                    .set(AGENT.TOTAL_PERMANENT_BALANCE, BigDecimalUtil.value(agentUpdate.getTotalPermanentBalance()))
                    .set(AGENT.DEBT_CREDIT, BigDecimalUtil.value(agentUpdate.getDebtCredit()))
                    .set(AGENT.EXTRA_DEBT_CREDIT, BigDecimalUtil.value(agentUpdate.getExtraDebtCredit()))
                    .set(AGENT.STATUS, StatusEnum.getEnum(agentUpdate.getStatus()).getId())
                    .set(AGENT.USER_NAME, agentUpdate.getUserName())
                    .set(AGENT.PASSWORD, agentUpdate.getPassword())
                    .set(AGENT.LAST_LOGIN, DateUtil.getDate(agentUpdate.getLastLogin()))
                    .set(AGENT.CREATION_DATE, DateUtil.getDate(agentUpdate.getCreationDate()))
                    .set(AGENT.TYPE, agentUpdate.getType())
                    .set(AGENT.OPERATOR_ID, agentUpdate.getOperatorId())
                    .where(AGENT.ID.equal(id))
                    .returning()
                    .fetchOne();
            if (agentRecord == null) throw new AgentNotFoundException(id);
            return agentMapper.toAgentUpdateDto(agentRecord);
        } catch (Exception ex) {
            ExceptionUtil.agentFkeyException(ex, agentUpdate.getCities(), agentUpdate.getOperatorId());
            throw new RuntimeException();
        }
    }

    @Override
    public void deleteById(Long id) {
        int[] batch = dslContext.batch(
                        dslContext.update(AGENT)
                                .set(AGENT.DELETE, true)
                                .where(AGENT.DELETE.equal(false)
                                        .and(AGENT.ID.equal(id))),
                        dslContext.update(CASHIER)
                                .set(CASHIER.DELETE, true)
                                .where(CASHIER.DELETE.equal(false)
                                        .and(CASHIER.AGENT_ID.equal(id)))
                )
                .execute();
        if (batch[0] == 0) throw new AgentNotFoundException(id);
    }
}
