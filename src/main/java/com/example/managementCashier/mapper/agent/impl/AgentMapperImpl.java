package com.example.managementCashier.mapper.agent.impl;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.mapper.agent.AgentMapper;
import com.example.managementCashier.util.DateUtil;
import jooq.tables.records.AgentRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jooq.tables.Agent.AGENT;
import static jooq.tables.Cashier.CASHIER;

public class AgentMapperImpl implements AgentMapper {

    @Override
    public List<AgentDto> toAgentDto(Result<Record> recordResult) {
        Map<Long, AgentDto> agentMap = new HashMap<>();

        recordResult.forEach(r -> {
            AgentDto agent = agentMap.get(r.getValue(AGENT.ID));
            if (agent == null) {
                agent = new AgentDto();
                agent.setId(r.getValue(AGENT.ID));
                agent.setAgentCode(r.getValue(AGENT.AGENT_CODE));
                agent.setFullName(r.getValue(AGENT.FULL_NAME));
                agent.setIdNumber(r.getValue(AGENT.ID_NUMBER));
                agent.setVoen(r.getValue(AGENT.VOEN));
                agent.setPhone(r.getValue(AGENT.PHONE));
                agent.setMobile(r.getValue(AGENT.MOBILE));
                agent.setEmail(r.getValue(AGENT.EMAIL));
                agent.setSalesRepresentative(r.getValue(AGENT.SALES_REPRESENTATIVE));
                agent.setCities(r.getValue(AGENT.CITIES));
                agent.setAddress(r.getValue(AGENT.ADDRESS));
                agent.setTotalPermanentBalance(r.getValue(AGENT.TOTAL_PERMANENT_BALANCE));
                agent.setDebtCredit(r.getValue(AGENT.DEBT_CREDIT));
                agent.setExtraDebtCredit(r.getValue(AGENT.EXTRA_DEBT_CREDIT));
                agent.setStatus(StatusEnum.getEnum(r.getValue(AGENT.STATUS)).name());
                agent.setUserName(r.getValue(AGENT.USER_NAME));
                agent.setPassword(r.getValue(AGENT.PASSWORD));
                agent.setLastLogin(DateUtil.getLong(r.getValue(AGENT.LAST_LOGIN)));
                agent.setCreationDate(DateUtil.getLong(r.getValue(AGENT.CREATION_DATE)));
                agent.setType(r.getValue(AGENT.TYPE));
                agent.setOperatorId(r.getValue(AGENT.OPERATOR_ID));
                agentMap.put(agent.getId(), agent);
            }

            if (r.getValue(CASHIER.ID) != null) {
                CashierDto cashier = new CashierDto();
                cashier.setId(r.getValue(CASHIER.ID));
                cashier.setCashierCode(r.getValue(CASHIER.CASHIER_CODE));
                cashier.setProviderId(r.getValue(CASHIER.PROVIDER_ID));
                cashier.setFullName(r.getValue(CASHIER.FULL_NAME));
                cashier.setMobile(r.getValue(CASHIER.MOBILE));
                cashier.setPhone(r.getValue(CASHIER.PHONE));
                cashier.setEmail(r.getValue(CASHIER.EMAIL));
                cashier.setSalesRepresentative(r.getValue(CASHIER.SALES_REPRESENTATIVE));
                cashier.setZone(r.getValue(CASHIER.ZONE));
                cashier.setCities(r.getValue(CASHIER.CITIES));
                cashier.setRegion(r.getValue(CASHIER.REGION));
                cashier.setAddress(r.getValue(CASHIER.ADDRESS));
                cashier.setMacAddress(r.getValue(CASHIER.MAC_ADDRESS));
                cashier.setNextPermanentBalance(r.getValue(CASHIER.NEXT_PERMANENT_BALANCE));
                cashier.setCurrentBalance(r.getValue(CASHIER.CURRENT_BALANCE));
                cashier.setDebtCredit(r.getValue(CASHIER.DEBT_CREDIT));
                cashier.setExtraDebtCredit(r.getValue(CASHIER.EXTRA_DEBT_CREDIT));
                cashier.setMinStake(r.getValue(CASHIER.MIN_STAKE));
                cashier.setMaxStake(r.getValue(CASHIER.MAX_STAKE));
                cashier.setBetTicketPayoutLimit(r.getValue(CASHIER.BET_TICKET_PAYOUT_LIMIT));
                cashier.setVoucherPayoutLimit(r.getValue(CASHIER.VOUCHER_PAYOUT_LIMIT));
                cashier.setStatus(StatusEnum.getEnum(r.getValue(CASHIER.STATUS)).name());
                cashier.setFreezeStatus(r.getValue(CASHIER.FREEZE_STATUS));
                cashier.setUserName(r.getValue(CASHIER.USER_NAME));
                cashier.setPassword(r.getValue(CASHIER.PASSWORD));
                cashier.setLastLogin(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGIN)));
                cashier.setLastLogout(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGOUT)));
                cashier.setCreationDate(DateUtil.getLong(r.getValue(CASHIER.CREATION_DATE)));
                cashier.setAgentId(r.getValue(CASHIER.AGENT_ID));
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        });
        return new ArrayList<>(agentMap.values());
    }

    @Override
    public AgentSaveDto toAgentSaveDto(AgentRecord agentRecord) {
        return new AgentSaveDto(
                agentRecord.getAgentCode(),
                agentRecord.getFullName(),
                agentRecord.getIdNumber(),
                agentRecord.getVoen(),
                agentRecord.getPhone(),
                agentRecord.getMobile(),
                agentRecord.getEmail(),
                agentRecord.getSalesRepresentative(),
                agentRecord.getCities(),
                agentRecord.getAddress(),
                agentRecord.getTotalPermanentBalance(),
                agentRecord.getDebtCredit(),
                agentRecord.getExtraDebtCredit(),
                agentRecord.getStatus(),
                agentRecord.getUserName(),
                agentRecord.getPassword(),
                DateUtil.getLong(agentRecord.getLastLogin()),
                DateUtil.getLong(agentRecord.getCreationDate()),
                agentRecord.getType(),
                agentRecord.getOperatorId()
        );
    }

    @Override
    public AgentUpdateDto toAgentUpdateDto(AgentRecord agentRecord) {
        return new AgentUpdateDto(
                agentRecord.getId(),
                agentRecord.getAgentCode(),
                agentRecord.getFullName(),
                agentRecord.getIdNumber(),
                agentRecord.getVoen(),
                agentRecord.getPhone(),
                agentRecord.getMobile(),
                agentRecord.getEmail(),
                agentRecord.getSalesRepresentative(),
                agentRecord.getCities(),
                agentRecord.getAddress(),
                agentRecord.getTotalPermanentBalance(),
                agentRecord.getDebtCredit(),
                agentRecord.getExtraDebtCredit(),
                agentRecord.getStatus(),
                agentRecord.getUserName(),
                agentRecord.getPassword(),
                DateUtil.getLong(agentRecord.getLastLogin()),
                DateUtil.getLong(agentRecord.getCreationDate()),
                agentRecord.getType(),
                agentRecord.getOperatorId()
        );
    }

    @Override
    public AgentNameDto toAgentNameDto(String agentName) {
        return new AgentNameDto(agentName);
    }

    private void addCashierList(List<CashierDto> cashierList, AgentDto agentDto, CashierDto cashierDto) {
        if (cashierList == null) {
            cashierList = new ArrayList<>();
        }
        cashierList.add(cashierDto);
        agentDto.setCashierList(cashierList);
    }
}
