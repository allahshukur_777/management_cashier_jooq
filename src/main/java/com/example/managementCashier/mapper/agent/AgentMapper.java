package com.example.managementCashier.mapper.agent;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import jooq.tables.records.AgentRecord;
import org.jooq.Record;
import org.jooq.Result;
import java.util.List;

public interface AgentMapper {

    List<AgentDto> toAgentDto(Result<Record> agentRecord);

    AgentSaveDto toAgentSaveDto(AgentRecord agentRecord);

    AgentUpdateDto toAgentUpdateDto(AgentRecord agentRecord);

    AgentNameDto toAgentNameDto(String agentName);
}
