package com.example.managementCashier.mapper.cashier.impl;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.mapper.cashier.CashierMapper;
import com.example.managementCashier.util.DateUtil;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;

public class CashierMapperImpl implements CashierMapper {

    @Override
    public CashierDto toCashierDto(Cashier cashier) {
        return new CashierDto(
                cashier.getId(),
                cashier.getCashierCode(),
                cashier.getProviderId(),
                cashier.getFullName(),
                cashier.getMobile(),
                cashier.getPhone(),
                cashier.getEmail(),
                cashier.getSalesRepresentative(),
                cashier.getZone(),
                cashier.getCities(),
                cashier.getRegion(),
                cashier.getAddress(),
                cashier.getMacAddress(),
                cashier.getNextPermanentBalance(),
                cashier.getCurrentBalance(),
                cashier.getDebtCredit(),
                cashier.getExtraDebtCredit(),
                cashier.getMinStake(),
                cashier.getMaxStake(),
                cashier.getBetTicketPayoutLimit(),
                cashier.getVoucherPayoutLimit(),
                StatusEnum.getEnum(cashier.getStatus()).name(),
                cashier.getFreezeStatus(),
                cashier.getUserName(),
                cashier.getPassword(),
                DateUtil.getLong(cashier.getLastLogin()),
                DateUtil.getLong(cashier.getLastLogout()),
                DateUtil.getLong(cashier.getCreationDate()),
                cashier.getAgentId());
    }

    @Override
    public CashierSaveDto toCashierSaveDto(CashierRecord cashierRecord) {
        return new CashierSaveDto(
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                cashierRecord.getAgentId());
    }

    @Override
    public CashierUpdateDto toCashierUpdateDto(CashierRecord cashierRecord) {
        return new CashierUpdateDto(
                cashierRecord.getId(),
                cashierRecord.getCashierCode(),
                cashierRecord.getProviderId(),
                cashierRecord.getFullName(),
                cashierRecord.getMobile(),
                cashierRecord.getPhone(),
                cashierRecord.getEmail(),
                cashierRecord.getSalesRepresentative(),
                cashierRecord.getZone(),
                cashierRecord.getCities(),
                cashierRecord.getRegion(),
                cashierRecord.getAddress(),
                cashierRecord.getMacAddress(),
                cashierRecord.getNextPermanentBalance(),
                cashierRecord.getCurrentBalance(),
                cashierRecord.getDebtCredit(),
                cashierRecord.getExtraDebtCredit(),
                cashierRecord.getMinStake(),
                cashierRecord.getMaxStake(),
                cashierRecord.getBetTicketPayoutLimit(),
                cashierRecord.getVoucherPayoutLimit(),
                cashierRecord.getStatus(),
                cashierRecord.getFreezeStatus(),
                cashierRecord.getUserName(),
                cashierRecord.getPassword(),
                DateUtil.getLong(cashierRecord.getLastLogin()),
                DateUtil.getLong(cashierRecord.getLastLogout()),
                DateUtil.getLong(cashierRecord.getCreationDate()),
                cashierRecord.getAgentId());
    }
}