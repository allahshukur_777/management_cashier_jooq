package com.example.managementCashier.mapper.cashier;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import jooq.tables.pojos.Cashier;
import jooq.tables.records.CashierRecord;

public interface CashierMapper {

    CashierDto toCashierDto(Cashier cashier);

    CashierSaveDto toCashierSaveDto(CashierRecord cashierRecord);

    CashierUpdateDto toCashierUpdateDto(CashierRecord cashierRecord);


}
