package com.example.managementCashier.mapper.operator;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import jooq.tables.records.OperatorRecord;
import org.jooq.Record;
import org.jooq.Result;
import java.util.List;

public interface OperatorMapper {

    List<OperatorDto> toOperatorDto(Result<Record> operatorRecord);

    OperatorSaveDto toOperatorSaveDto(OperatorRecord operatorRecord);

    OperatorUpdateDto toOperatorUpdateDto(OperatorRecord operatorRecord);
}
