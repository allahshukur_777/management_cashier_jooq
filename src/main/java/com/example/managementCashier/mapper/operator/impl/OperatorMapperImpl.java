package com.example.managementCashier.mapper.operator.impl;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.enums.StatusEnum;
import com.example.managementCashier.mapper.operator.OperatorMapper;
import com.example.managementCashier.util.DateUtil;
import jooq.tables.records.OperatorRecord;
import org.jooq.Record;
import org.jooq.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static jooq.tables.Operator.OPERATOR;
import static jooq.tables.Agent.AGENT;
import static jooq.tables.Cashier.CASHIER;

public class OperatorMapperImpl implements OperatorMapper {

    @Override
    public List<OperatorDto> toOperatorDto(Result<Record> recordResult) {
        Map<Long, OperatorDto> operatorMap = new HashMap<>();
        Map<Long, AgentDto> agentMap = new HashMap<>();

        recordResult.forEach(r -> {
            OperatorDto operator = operatorMap.get(r.getValue(OPERATOR.ID));
            if (operator == null) {
                operator = new OperatorDto();
                operator.setId(r.getValue(OPERATOR.ID));
                operator.setName(r.getValue(OPERATOR.NAME));
                operator.setHeadquarterAddress(r.getValue(OPERATOR.HEADQUARTER_ADDRESS));
                operator.setEmailAddress(r.getValue(OPERATOR.EMAIL_ADDRESS));
                operator.setWebAddress(r.getValue(OPERATOR.WEB_ADDRESS));
                operator.setPhoneNumber(r.getValue(OPERATOR.PHONE_NUMBER));
                operator.setFaxNumber(r.getValue(OPERATOR.FAX_NUMBER));
                operator.setMaxStake(r.getValue(OPERATOR.MAX_STAKE));
                operator.setMinStake(r.getValue(OPERATOR.MIN_STAKE));
                operator.setPayoutLimit(r.getValue(OPERATOR.PAYOUT_LIMIT));
                operator.setMaxIssueAmountPerVoucher(r.getValue(OPERATOR.MAX_ISSUE_AMOUNT_PER_VOUCHER));
                operator.setAvailableCreditLimit(r.getValue(OPERATOR.AVAILABLE_CREDIT_LIMIT));
                operator.setTicketExpirationPeriod(r.getValue(OPERATOR.TICKET_EXPIRATION_PERIOD));
                operator.setStatus(StatusEnum.getEnum(r.getValue(OPERATOR.STATUS)).name());
                operator.setCreationDate(DateUtil.getLong(r.getValue(OPERATOR.CREATION_DATE)));
                operator.setType(r.getValue(OPERATOR.TYPE));
                operatorMap.put(operator.getId(), operator);
            }

            AgentDto agent = agentMap.get(r.getValue(AGENT.ID));
            if (agent == null && r.getValue(AGENT.ID) != null) {
                agent = new AgentDto();
                agent.setId(r.getValue(AGENT.ID));
                agent.setAgentCode(r.getValue(AGENT.AGENT_CODE));
                agent.setFullName(r.getValue(AGENT.FULL_NAME));
                agent.setIdNumber(r.getValue(AGENT.ID_NUMBER));
                agent.setVoen(r.getValue(AGENT.VOEN));
                agent.setPhone(r.getValue(AGENT.PHONE));
                agent.setMobile(r.getValue(AGENT.MOBILE));
                agent.setEmail(r.getValue(AGENT.EMAIL));
                agent.setSalesRepresentative(r.getValue(AGENT.SALES_REPRESENTATIVE));
                agent.setCities(r.getValue(AGENT.CITIES));
                agent.setAddress(r.getValue(AGENT.ADDRESS));
                agent.setTotalPermanentBalance(r.getValue(AGENT.TOTAL_PERMANENT_BALANCE));
                agent.setDebtCredit(r.getValue(AGENT.DEBT_CREDIT));
                agent.setExtraDebtCredit(r.getValue(AGENT.EXTRA_DEBT_CREDIT));
                agent.setStatus(StatusEnum.getEnum(r.getValue(AGENT.STATUS)).name());
                agent.setUserName(r.getValue(AGENT.USER_NAME));
                agent.setPassword(r.getValue(AGENT.PASSWORD));
                agent.setLastLogin(DateUtil.getLong(r.getValue(AGENT.LAST_LOGIN)));
                agent.setCreationDate(DateUtil.getLong(r.getValue(AGENT.CREATION_DATE)));
                agent.setType(r.getValue(AGENT.TYPE));
                agent.setOperatorId(r.getValue(AGENT.OPERATOR_ID));
                agentMap.put(agent.getId(), agent);
                addAgentList(operator.getAgentList(), operator, agent);
            }

            if (r.getValue(CASHIER.ID) != null) {
                CashierDto cashier = new CashierDto();
                cashier.setId(r.getValue(CASHIER.ID));
                cashier.setCashierCode(r.getValue(CASHIER.CASHIER_CODE));
                cashier.setProviderId(r.getValue(CASHIER.PROVIDER_ID));
                cashier.setFullName(r.getValue(CASHIER.FULL_NAME));
                cashier.setMobile(r.getValue(CASHIER.MOBILE));
                cashier.setPhone(r.getValue(CASHIER.PHONE));
                cashier.setEmail(r.getValue(CASHIER.EMAIL));
                cashier.setSalesRepresentative(r.getValue(CASHIER.SALES_REPRESENTATIVE));
                cashier.setZone(r.getValue(CASHIER.ZONE));
                cashier.setCities(r.getValue(CASHIER.CITIES));
                cashier.setRegion(r.getValue(CASHIER.REGION));
                cashier.setAddress(r.getValue(CASHIER.ADDRESS));
                cashier.setMacAddress(r.getValue(CASHIER.MAC_ADDRESS));
                cashier.setNextPermanentBalance(r.getValue(CASHIER.NEXT_PERMANENT_BALANCE));
                cashier.setCurrentBalance(r.getValue(CASHIER.CURRENT_BALANCE));
                cashier.setDebtCredit(r.getValue(CASHIER.DEBT_CREDIT));
                cashier.setExtraDebtCredit(r.getValue(CASHIER.EXTRA_DEBT_CREDIT));
                cashier.setMinStake(r.getValue(CASHIER.MIN_STAKE));
                cashier.setMaxStake(r.getValue(CASHIER.MAX_STAKE));
                cashier.setBetTicketPayoutLimit(r.getValue(CASHIER.BET_TICKET_PAYOUT_LIMIT));
                cashier.setVoucherPayoutLimit(r.getValue(CASHIER.VOUCHER_PAYOUT_LIMIT));
                cashier.setStatus(StatusEnum.getEnum(r.getValue(CASHIER.STATUS)).name());
                cashier.setFreezeStatus(r.getValue(CASHIER.FREEZE_STATUS));
                cashier.setUserName(r.getValue(CASHIER.USER_NAME));
                cashier.setPassword(r.getValue(CASHIER.PASSWORD));
                cashier.setLastLogin(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGIN)));
                cashier.setLastLogout(DateUtil.getLong(r.getValue(CASHIER.LAST_LOGOUT)));
                cashier.setCreationDate(DateUtil.getLong(r.getValue(CASHIER.CREATION_DATE)));
                cashier.setAgentId(r.getValue(CASHIER.AGENT_ID));
                assert agent != null;
                addCashierList(agent.getCashierList(), agent, cashier);
            }
        });
        return new ArrayList<>(operatorMap.values());
    }

    @Override
    public OperatorSaveDto toOperatorSaveDto(OperatorRecord operatorRecord) {
        return new OperatorSaveDto(
                operatorRecord.getName(),
                operatorRecord.getType(),
                operatorRecord.getHeadquarterAddress(),
                operatorRecord.getEmailAddress(),
                operatorRecord.getWebAddress(),
                operatorRecord.getPhoneNumber(),
                operatorRecord.getFaxNumber(),
                operatorRecord.getMaxStake(),
                operatorRecord.getMinStake(),
                operatorRecord.getPayoutLimit(),
                operatorRecord.getMaxIssueAmountPerVoucher(),
                operatorRecord.getAvailableCreditLimit(),
                operatorRecord.getTicketExpirationPeriod(),
                operatorRecord.getStatus(),
                DateUtil.getLong(operatorRecord.getCreationDate())
        );
    }

    @Override
    public OperatorUpdateDto toOperatorUpdateDto(OperatorRecord operatorRecord) {
        return new OperatorUpdateDto(
                operatorRecord.getId(),
                operatorRecord.getName(),
                operatorRecord.getType(),
                operatorRecord.getHeadquarterAddress(),
                operatorRecord.getEmailAddress(),
                operatorRecord.getWebAddress(),
                operatorRecord.getPhoneNumber(),
                operatorRecord.getFaxNumber(),
                operatorRecord.getMaxStake(),
                operatorRecord.getMinStake(),
                operatorRecord.getPayoutLimit(),
                operatorRecord.getMaxIssueAmountPerVoucher(),
                operatorRecord.getAvailableCreditLimit(),
                operatorRecord.getTicketExpirationPeriod(),
                operatorRecord.getStatus(),
                DateUtil.getLong(operatorRecord.getCreationDate())
        );
    }

    private void addAgentList(List<AgentDto> agentList, OperatorDto operatorDto, AgentDto agentDto) {
        if (agentList == null) {
            agentList = new ArrayList<>();
        }
        agentList.add(agentDto);
        operatorDto.setAgentList(agentList);
    }

    private void addCashierList(List<CashierDto> cashierList, AgentDto agentDto, CashierDto cashierDto) {
        if (cashierList == null) {
            cashierList = new ArrayList<>();
        }
        cashierList.add(cashierDto);
        agentDto.setCashierList(cashierList);
    }
}
