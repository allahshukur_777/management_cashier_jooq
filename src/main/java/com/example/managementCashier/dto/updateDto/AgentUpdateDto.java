package com.example.managementCashier.dto.updateDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentUpdateDto {
    private Long id;
    @NotNull(message = "Agent Code can not be null")
    private Long agentCode;
    @NotBlank(message = "Full Name can not be null")
    private String fullName;
    private Long idNumber;
    @NotNull(message = "Voen can not be null")
    private Long voen;
    @NotBlank(message = "Phone can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Phone can be (+000)00 000-00-00")
    private String phone;
    @NotBlank(message = "Mobile can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Mobile can be (+000)00 000-00-00")
    private String mobile;
    @NotBlank(message = "Email can not be null")
    @Email(message = "Email should be valid")
    private String email;
    private String salesRepresentative;
    @NotNull(message = "Cities can not be null")
    private Long cities;
    private String address;
    private BigDecimal totalPermanentBalance;
    private BigDecimal debtCredit;
    private BigDecimal extraDebtCredit;
    @NotNull(message = "Status can not be null")
    private int status;
    private String userName;
    private String password;
    @NotNull(message = "Last Login can not be null")
    private Long lastLogin;
    @NotNull(message = "Creation Date can not be null")
    private Long creationDate;
    @NotBlank(message = "Type can not be null")
    private String type;
    @NotNull(message = "Operator Id can not be null")
    private Long operatorId;
}
