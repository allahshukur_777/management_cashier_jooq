package com.example.managementCashier.dto.saveDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperatorSaveDto {
    @NotBlank(message = "Name can not be null")
    private String name;
    @NotBlank(message = "Type can not be null")
    private String type;
    @NotBlank(message = "Headquarter Address can not be null")
    private String headquarterAddress;
    @NotBlank(message = "Email Address can not be null")
    @Email(message = "Email should be valid")
    private String emailAddress;
    @NotBlank(message = "Web Address can not be null")
    private String webAddress;
    @NotBlank(message = "Phone Number can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Phone Number can be (+000)00 000-00-00")
    private String phoneNumber;
    @NotBlank(message = "Fax Number can not be null")
    private String faxNumber;
    private BigDecimal maxStake;
    private BigDecimal minStake;
    private BigDecimal payoutLimit;
    private BigDecimal maxIssueAmountPerVoucher;
    private BigDecimal availableCreditLimit;
    private BigDecimal ticketExpirationPeriod;
    private int status;
    @NotNull(message = "Creation Date can not be null")
    private Long creationDate;
}
