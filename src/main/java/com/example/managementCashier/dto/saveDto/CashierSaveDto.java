package com.example.managementCashier.dto.saveDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CashierSaveDto {
    @NotNull(message = "Cashier Code can not be null")
    private Long cashierCode;
    @NotBlank(message = "Provider Id can not be null")
    @Size(min = 10, max = 10, message = "Provider Id can be 10 symbol")
    private String providerId;
    @NotBlank(message = "Full Name can not be null")
    private String fullName;
    @NotBlank(message = "Mobile can not be null")
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Mobile can be (+000)00 000-00-00")
    private String mobile;
    @Pattern(regexp = "[(][^[+][0-9]]{4}[)][0-9]{2}[\\s][0-9]{3}[^[-][0-9]]{3}[^[-][0-9]]{3}", message = "Phone can be (+000)00 000-00-00")
    private String phone;
    @NotBlank(message = "Email can not be null")
    @Email(message = "Email should be valid")
    private String email;
    private String salesRepresentative;
    @NotNull(message = "Zone can not be null")
    private Long zone;
    @NotNull(message = "Cities can not be null")
    private Long cities;
    @NotNull(message = "Region can not be null")
    private Long region;
    @NotBlank(message = "Address can not be null")
    private String address;
    private String macAddress;
    private BigDecimal nextPermanentBalance;
    private BigDecimal currentBalance;
    private BigDecimal debtCredit;
    private BigDecimal extraDebtCredit;
    private BigDecimal minStake;
    private BigDecimal maxStake;
    private BigDecimal betTicketPayoutLimit;
    private BigDecimal voucherPayoutLimit;
    @NotNull(message = "Status can not be null")
    private int status;
    @NotNull(message = "Freeze Status can not be null")
    private Long freezeStatus;
    private String userName;
    private String password;
    @NotNull(message = "Last Login can not be null")
    private Long lastLogin;
    @NotNull(message = "Last Logout can not be null")
    private Long lastLogout;
    @NotNull(message = "Creation Date can not be null")
    private Long creationDate;
    @NotNull(message = "Agent Id can not be null")
    private Long agentId;
}
