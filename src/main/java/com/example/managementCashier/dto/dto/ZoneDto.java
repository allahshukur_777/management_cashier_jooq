package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class ZoneDto {
    private Long id;
    private String name;
}
