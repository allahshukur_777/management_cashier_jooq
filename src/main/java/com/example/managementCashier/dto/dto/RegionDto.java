package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class RegionDto {
    private Long id;
    private String name;
}
