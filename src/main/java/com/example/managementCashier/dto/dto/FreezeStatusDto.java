package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class FreezeStatusDto {
    private Long id;
    private String name;
}
