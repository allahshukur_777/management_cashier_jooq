package com.example.managementCashier.dto.dto;

import lombok.Data;

@Data
public class CitiesDto {
    private Long id;
    private String name;
}
