package com.example.managementCashier.dto.dto;

import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public class OperatorDto {
    private Long id;
    private String name;
    private String type;
    private String headquarterAddress;
    private String emailAddress;
    private String webAddress;
    private String phoneNumber;
    private String faxNumber;
    private BigDecimal maxStake;
    private BigDecimal minStake;
    private BigDecimal payoutLimit;
    private BigDecimal maxIssueAmountPerVoucher;
    private BigDecimal availableCreditLimit;
    private BigDecimal ticketExpirationPeriod;
    private String status;
    private Long creationDate;
    private List<AgentDto> agentList;
}
