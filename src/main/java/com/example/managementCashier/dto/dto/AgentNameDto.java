package com.example.managementCashier.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentNameDto {
    @NotBlank(message = "Name can not be null")
    private String name;
}
