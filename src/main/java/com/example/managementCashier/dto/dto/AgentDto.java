package com.example.managementCashier.dto.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgentDto {
    private Long id;
    private Long agentCode;
    private String fullName;
    private Long idNumber;
    private Long voen;
    private String phone;
    private String mobile;
    private String email;
    private String salesRepresentative;
    private Long cities;
    private String address;
    private BigDecimal totalPermanentBalance;
    private BigDecimal debtCredit;
    private BigDecimal extraDebtCredit;
    private String status;
    private String userName;
    private String password;
    private Long lastLogin;
    private Long creationDate;
    private String type;
    private Long operatorId;
    private List<CashierDto> cashierList;
}
