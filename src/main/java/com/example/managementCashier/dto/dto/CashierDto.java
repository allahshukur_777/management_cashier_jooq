package com.example.managementCashier.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CashierDto {
    private Long id;
    private Long cashierCode;
    private String providerId;
    private String fullName;
    private String mobile;
    private String phone;
    private String email;
    private String salesRepresentative;
    private Long zone;
    private Long cities;
    private Long region;
    private String address;
    private String macAddress;
    private BigDecimal nextPermanentBalance;
    private BigDecimal currentBalance;
    private BigDecimal debtCredit;
    private BigDecimal extraDebtCredit;
    private BigDecimal minStake;
    private BigDecimal maxStake;
    private BigDecimal betTicketPayoutLimit;
    private BigDecimal voucherPayoutLimit;
    private String status;
    private Long freezeStatus;
    private String userName;
    private String password;
    private Long lastLogin;
    private Long lastLogout;
    private Long creationDate;
    private Long agentId;
}
