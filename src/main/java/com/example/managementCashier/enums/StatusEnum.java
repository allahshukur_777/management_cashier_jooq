package com.example.managementCashier.enums;

import com.example.managementCashier.exception.StatusNotFoundException;

public enum StatusEnum {

    ACTIVE(1), INACTIVE(2), DELETE(3);

    private final Integer id;

    StatusEnum(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public static StatusEnum getEnum(Integer value) {
        switch (value) {
            case 1:
                return ACTIVE;
            case 2:
                return INACTIVE;
            case 3:
                return DELETE;
            default:
                throw new StatusNotFoundException(value);
        }
    }
}

