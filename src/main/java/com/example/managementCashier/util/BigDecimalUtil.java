package com.example.managementCashier.util;

import java.math.BigDecimal;

public class BigDecimalUtil {

    public static BigDecimal value(BigDecimal bigDecimal) {
        return bigDecimal == null ? BigDecimal.valueOf(0) : bigDecimal;
    }
}
