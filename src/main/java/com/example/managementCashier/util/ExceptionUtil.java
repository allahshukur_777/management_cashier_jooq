package com.example.managementCashier.util;

import com.example.managementCashier.exception.AgentNotFoundException;
import com.example.managementCashier.exception.CitiesNotFoundException;
import com.example.managementCashier.exception.FreezeStatusNotFoundException;
import com.example.managementCashier.exception.OperatorNotFoundException;
import com.example.managementCashier.exception.RegionNotFoundException;
import com.example.managementCashier.exception.StatusNotFoundException;
import com.example.managementCashier.exception.ZoneNotFoundException;

public class ExceptionUtil {

    public static void cashierFkeyException(Exception ex, Long zoneId, Long citiesId, Long regionId,
                                            Long freezeId, Long agentId) {
        if (ex.getMessage().contains("cashier_zone_fkey"))
            throw new ZoneNotFoundException(zoneId);
        if (ex.getMessage().contains("cashier_cities_fkey"))
            throw new CitiesNotFoundException(citiesId);
        if (ex.getMessage().contains("cashier_region_fkey"))
            throw new RegionNotFoundException(regionId);
        if (ex.getMessage().contains("cashier_freeze_fkey"))
            throw new FreezeStatusNotFoundException(freezeId);
        if (ex.getMessage().contains("cashier_agent_fkey"))
            throw new AgentNotFoundException(agentId);
    }

    public static void agentFkeyException(Exception ex, Long citiesId, Long operatorId) {
        if (ex.getMessage().contains("agent_cities_fkey"))
            throw new CitiesNotFoundException(citiesId);
        if (ex.getMessage().contains("operator_agent_fkey"))
            throw new OperatorNotFoundException(operatorId);
    }
}
