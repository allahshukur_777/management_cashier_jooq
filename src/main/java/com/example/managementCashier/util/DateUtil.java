package com.example.managementCashier.util;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static DateTimeFormatter formatter() {
        return DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public static String dateFormat(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(date);
    }

    public static Long getLong(Date date){
        return date.getTime();
    }

    public static Date getDate(Long dateLong){
        LocalDate localDate = Instant.ofEpochMilli(dateLong).atZone(ZoneId.systemDefault()).toLocalDate();
        return Date.valueOf(localDate);
    }
}
