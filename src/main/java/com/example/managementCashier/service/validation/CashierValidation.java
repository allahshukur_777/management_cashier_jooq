package com.example.managementCashier.service.validation;

import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;

public interface CashierValidation {

    CashierSaveDto cashierValidateSave(CashierSaveDto cashierSave);

    CashierUpdateDto cashierValidateUpdate(Long id, CashierUpdateDto cashierUpdate);
}
