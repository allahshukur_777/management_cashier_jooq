package com.example.managementCashier.service.validation.impl;

import com.example.managementCashier.service.validation.CashierValidation;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CashierValidationImpl implements CashierValidation {


    @Override
    public CashierSaveDto cashierValidateSave(CashierSaveDto cashierSave) {
        return null;
    }

    @Override
    public CashierUpdateDto cashierValidateUpdate(Long id, CashierUpdateDto cashierUpdate) {
        return null;
    }
}
