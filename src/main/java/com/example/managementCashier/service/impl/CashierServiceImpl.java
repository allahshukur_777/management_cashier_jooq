package com.example.managementCashier.service.impl;

import com.example.managementCashier.service.validation.CashierValidation;
import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import com.example.managementCashier.repository.CashierRepository;
import com.example.managementCashier.service.CashierService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CashierServiceImpl implements CashierService {

    private final CashierRepository cashierRepository;
    private final CashierValidation cashierValidation;

    @Override
    public List<CashierDto> getAll(Pageable pageable) {
        return cashierRepository.findAll(pageable);
    }

    @Override
    public CashierDto get(Long id) {
        return cashierRepository.findById(id);
    }

    @Override
    public CashierSaveDto save(CashierSaveDto cashierSave){
        return cashierRepository.save(cashierSave);
    }

    @Override
    public CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdate) {
        return cashierRepository.update(id, cashierUpdate);
    }

    @Override
    public void deleteById(Long id) {
         cashierRepository.deleteById(id);
    }

}
