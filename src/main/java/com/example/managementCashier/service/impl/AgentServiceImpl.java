package com.example.managementCashier.service.impl;

import com.example.managementCashier.dto.dto.AgentDto;
import com.example.managementCashier.dto.dto.AgentNameDto;
import com.example.managementCashier.dto.saveDto.AgentSaveDto;
import com.example.managementCashier.dto.updateDto.AgentUpdateDto;
import com.example.managementCashier.repository.AgentRepository;
import com.example.managementCashier.service.AgentService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AgentServiceImpl implements AgentService {

    private final AgentRepository agentRepository;

    @Override
    public List<AgentDto> getAll(Pageable pageable) {
        return agentRepository.findAll(pageable);
    }

    @Override
    public AgentDto get(Long id) {
        return agentRepository.findById(id);
    }

    @Override
    public AgentNameDto getName(Long id) {
        return agentRepository.findName(id);
    }

    @Override
    public AgentSaveDto save(AgentSaveDto agentSave) {
        return agentRepository.save(agentSave);
    }

    @Override
    public AgentUpdateDto update(Long id, AgentUpdateDto agentUpdate) {
        return agentRepository.update(id, agentUpdate);
    }

    @Override
    public void deleteById(Long id) {
        agentRepository.deleteById(id);
    }
}
