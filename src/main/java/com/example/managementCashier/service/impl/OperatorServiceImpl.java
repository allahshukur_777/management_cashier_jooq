package com.example.managementCashier.service.impl;

import com.example.managementCashier.dto.dto.OperatorDto;
import com.example.managementCashier.dto.saveDto.OperatorSaveDto;
import com.example.managementCashier.dto.updateDto.OperatorUpdateDto;
import com.example.managementCashier.repository.OperatorRepository;
import com.example.managementCashier.service.OperatorService;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OperatorServiceImpl implements OperatorService {

    private final OperatorRepository operatorRepository;

    @Override
    public List<OperatorDto> getAll(Pageable pageable) {
        return operatorRepository.findAll(pageable);
    }

    @Override
    public OperatorDto get(Long id) {
        return operatorRepository.findById(id);
    }

    @Override
    public OperatorSaveDto save(OperatorSaveDto operatorSave) {
        return operatorRepository.save(operatorSave);
    }

    @Override
    public OperatorUpdateDto update(Long id, OperatorUpdateDto operatorUpdate) {
        return operatorRepository.update(id, operatorUpdate);
    }

    @Override
    public void deleteById(Long id) {
        operatorRepository.deleteById(id);
    }
}
