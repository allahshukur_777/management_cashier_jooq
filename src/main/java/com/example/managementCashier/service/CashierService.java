package com.example.managementCashier.service;

import com.example.managementCashier.dto.dto.CashierDto;
import com.example.managementCashier.dto.saveDto.CashierSaveDto;
import com.example.managementCashier.dto.updateDto.CashierUpdateDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CashierService {

    List<CashierDto> getAll(Pageable pageable);

    CashierSaveDto save(CashierSaveDto cashierSaveDto);

    CashierUpdateDto update(Long id, CashierUpdateDto cashierUpdateDto);

    void deleteById(Long id);

    CashierDto get(Long id);
}
