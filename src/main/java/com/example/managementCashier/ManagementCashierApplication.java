package com.example.managementCashier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagementCashierApplication {

    public static void main(String[] args) throws Exception{
        SpringApplication.run(ManagementCashierApplication.class, args);
    }

}
