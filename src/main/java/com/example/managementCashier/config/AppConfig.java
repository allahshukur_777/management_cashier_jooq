package com.example.managementCashier.config;

import com.example.managementCashier.mapper.agent.impl.AgentMapperImpl;
import com.example.managementCashier.mapper.cashier.impl.CashierMapperImpl;
import com.example.managementCashier.mapper.operator.impl.OperatorMapperImpl;
import com.example.managementCashier.swagger.SwaggerPageable;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import static springfox.documentation.builders.PathSelectors.regex;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
@EnableSpringDataWebSupport
public class AppConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                .paths(regex("/.*"))
                .build()
                .directModelSubstitute(Pageable.class, SwaggerPageable.class);
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public OperatorMapperImpl getOperatorMapper() {
        return new OperatorMapperImpl();
    }

    @Bean
    public AgentMapperImpl getAgentMapper() {
        return new AgentMapperImpl();
    }

    @Bean
    public CashierMapperImpl getCashierMapper() {
        return new CashierMapperImpl();
    }

    @Override // This Override I have used for @PageableDefault
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new PageableHandlerMethodArgumentResolver());
    }
}
